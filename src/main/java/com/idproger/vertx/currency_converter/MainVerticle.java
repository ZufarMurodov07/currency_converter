package com.idproger.vertx.currency_converter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

import java.util.Date;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

  }

  public static void main(String[] args) {
    System.out.println(new Date().getTime());
  }
}
