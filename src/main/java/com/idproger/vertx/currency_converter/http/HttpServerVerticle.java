package com.idproger.vertx.currency_converter.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.codec.BodyCodec;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

public class HttpServerVerticle extends AbstractVerticle {

  public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";
  private static final Logger LOGGER = LoggerFactory.getLogger(HttpServerVerticle.class);

  private static final String CURRENCY_CONVERT_ENDPOINT = "http://apilayer.net/api/live";
  private static final String API_ACCESS_KEY = "0d542abe98c643ea10128e6cb419d7d6";

  private FreeMarkerTemplateEngine templateEngine;
  private WebClient webClient;

  private JsonArray currencies = new JsonArray();
//  private JsonArray result = new JsonArray();

  @Override
  public void start(Promise<Void> promise) throws Exception {

    InputStream is = getClass().getResourceAsStream("/currencies.json");
//    InputStream is = getClass().getResourceAsStream("/countries-final.json");
//    InputStream is2 = getClass().getResourceAsStream("/common-currency.json");

    String jsonTxt = null;
//    String currencyJsonTxt = null;
    try {
      jsonTxt = IOUtils.toString(is);
//      currencyJsonTxt = IOUtils.toString(is2);


      JsonObject jsonObj = new JsonObject(jsonTxt);
//      JsonObject curr = new JsonObject(currencyJsonTxt);

      currencies = jsonObj.getJsonArray("data");

      // Todo  for generate currencies.json
//      for (int i = 0; i < currencies.size(); i++) {
//        JsonObject obj = currencies.getJsonObject(i);
//        JsonObject _new = new JsonObject();
//        _new
//          .put("code", obj.getString("code"))
//          .put("name", obj.getString("name"))
//          .put("img", obj.getString("img"));
//
//        JsonObject currItem = curr.getJsonObject(obj.getString("code"));
//        if (currItem != null) {
//          _new
//            .put("symbol", currItem.getString("symbol"))
//            .put("symbol_native", currItem.getString("symbol_native"));
//        } else
//          _new
//            .put("symbol", "")
//            .put("symbol_native", "");
//
//        result.add(_new);
//      }

    } catch (IOException e) {
      System.out.println(e.getMessage());
    }

    HttpServer server = vertx.createHttpServer();

    webClient = WebClient.create(vertx, new WebClientOptions()
      .setUserAgent("vert-x3"));

    Router router = Router.router(vertx);
    router.get("/").handler(this::indexHandler);
    router.get("/app/*").handler(StaticHandler.create().setCachingEnabled(false));


    templateEngine = FreeMarkerTemplateEngine.create(vertx);

    Router apiRouter = Router.router(vertx);
    apiRouter.get("/currency").handler(this::apiCurrency);
    apiRouter.get("/convert/live").handler(this::apiConverter);
    router.mountSubRouter("/api", apiRouter);

    int portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 8080);
    server
      .requestHandler(router)
      .listen(portNumber, ar -> {
        if (ar.succeeded()) {
          LOGGER.info("HTTP server running on port " + portNumber);
          promise.complete();
        } else {
          LOGGER.error("Could not start a HTTP server", ar.cause());
          promise.fail(ar.cause());
        }
      });
  }

  private void indexHandler(RoutingContext context) {
    context.put("title", "Currency Converter");
//    context.put("pages", "{'status':200}");
//    context.put("countries", currencies);


    templateEngine.render(context.data(), "webroot/index.ftl", ar -> {
      if (ar.succeeded()) {
        context.response().putHeader("Content-Type", "text/html");
        context.response().end(ar.result());
      } else {
        context.fail(ar.cause());
      }
    });
  }

  private void apiCurrency(RoutingContext context) {
    JsonObject response = new JsonObject();
    if (currencies.size() != 0) {
      response
        .put("success", true)
        .put("currencies", currencies);
      context.response().setStatusCode(200);
      context.response().putHeader("Content-Type", "application/json");
      context.response().end(response.encode());
    } else {
      response
        .put("success", false)
        .put("error", "Currencies does not exists");
      context.response().setStatusCode(500);
      context.response().putHeader("Content-Type", "application/json");
      context.response().end(response.encode());
    }

  }

  private void apiConverter(RoutingContext context) {
    System.out.println("apiConverter");
    String source = context.request().getParam("source");
    String currency = context.request().getParam("currency");
    System.out.println("source: " + source);
    System.out.println("currency: " + currency);

    if (source != null && currency != null) {
      webClient
        .getAbs("http://api.currencylayer.com/live?access_key=0d542abe98c643ea10128e6cb419d7d6&currencies=" + currency + "&source=" + source + "&format=1")
        .as(BodyCodec.jsonObject())
        .send(ar -> {
          if (ar.succeeded()) {
            HttpResponse<JsonObject> response = ar.result();
            if (response.statusCode() == 200) {
              JsonObject body = response.body();
              body.put("key", source.concat(currency).toUpperCase());
              body.put("requested_at", new Date().getTime());
              context.response().setStatusCode(200);
              context.response().putHeader("Content-Type", "application/json");
              context.response().end(body.encode());
            } else {
              StringBuilder message = new StringBuilder()
                .append("Could not get data.")
                .append(response.statusMessage());
              JsonObject body = response.body();
              if (body != null) {
                message.append(System.getProperty("line.separator"))
                  .append(body.encodePrettily());
              }
              LOGGER.error(message.toString());
              context.response().setStatusCode(403);
              context.response().putHeader("Content-Type", "application/json");
              context.response().end(message.toString());
            }
          } else {
            Throwable err = ar.cause();
            LOGGER.error("HTTP Client error", err);
            context.fail(err);
          }
        });
    }

  }
}
