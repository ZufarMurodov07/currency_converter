package com.idproger.vertx.currency_converter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.ext.web.codec.BodyCodec;

public class CurrencyWorkerVerticle extends AbstractVerticle {

  private static final String CURRENCY_CONVERT_HOST = "apilayer.net";
  private static final String CURRENCY_CONVERT_ENDPOINT = "/api/live";
  private static final String ACCESS_KEY = "0d542abe98c643ea10128e6cb419d7d6";

  private String source = "USD";
  private String currencies = "UZS";

  private HttpRequest<JsonObject> request;


  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    request = WebClient
      .create(vertx)
      .getAbs("http://api.currencylayer.com/live?access_key=0d542abe98c643ea10128e6cb419d7d6&currencies=" +
        currencies + "&source=" + source + "&format=1")
//      .get(443, CURRENCY_CONVERT_HOST, CURRENCY_CONVERT_ENDPOINT)
//      .putHeader("Accept", "application/json")
//      .addQueryParam("access_key", ACCESS_KEY)
//      .addQueryParam("source", source)
//      .addQueryParam("currencies", currencies)
//      .addQueryParam("format", "1")
      .as(BodyCodec.jsonObject()) // (5)
      .expect(ResponsePredicate.SC_OK);

    vertx.setPeriodic(3000, id -> fetchData());
  }

  private void fetchData() {
    request.send(asyncResult -> {
      if (asyncResult.succeeded()) {
        System.out.println(asyncResult.result().body());
        System.out.println();
      }
    });
  }

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(new CurrencyWorkerVerticle());

  }
}
