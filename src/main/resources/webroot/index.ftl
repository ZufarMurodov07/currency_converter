<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/fontawesome.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500&amp;display=swap" as="style"
        onload="this.onload=null;this.rel='stylesheet'">
  <link rel="stylesheet" href="/app/main.css">

  <title>${title}</title>
</head>
<body>


<div class="container">

  <h1 class="title">BGN to EUR</h1>
  <p class="descr">Convert <span id="source-descr">Bulgarian Lev (BGN)</span> to <span id="currency-descr">Euro (EUR)</span> with the Currency Converter.com</p>

  <div class="content">
    <div class="left">
      <div class="selection-box">
        <div class="curr-selection s-left" >

          <div class="curr-flag" >
            <img class="curr-flag-image" src="https://valuta.exchange/img/flags/bgn-flag.png" alt="flag">
          </div>

          <span class="curr-name">
          Bulgarian Lev
          </span>
          <svg viewBox="0 0 24 24" fill="none" stroke="#1c1c1c" stroke-linecap="round" stroke-linejoin="round"
               stroke-width="2px">
            <path class="prefix__a" d="M15.28 14.88L12 18.13l-3.28-3.25M8.72 9.12L12 5.87l3.25 3.25"></path>
          </svg>

          <div class="selector-container d-none">
            <div class="curr-search">
              <svg viewBox="0 0 24 24" fill="none" stroke="#808FA1" stroke-linecap="round" stroke-linejoin="round"
                   stroke-width="2px">
                <circle cx="10.54" cy="10.54" r="5.11"></circle>
                <line x1="18.58" y1="18.58" x2="14.19" y2="14.19"></line>
              </svg>
              <input id="left-search-input" type="text" placeholder="Search" class="curr-search-input">
            </div>
            <div class="curr-list">

            </div>
          </div>
        </div>

      </div>

      <input id="left-input" type="text" value="1" class="curr-input">

      <div id="left-curr-display" class="curr-display">лв</div>
    </div>

    <button class="switch">
      <svg viewBox="0 0 24 24" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round"
           stroke-width="2px">
        <path d="M8.33 20.05l-2.9-2.91 2.9-3"></path>
        <path d="M18.57 12.71v1.48a2.91 2.91 0 01-2.89 2.91H5.43M15.67 4l2.9 3-2.9 2.91"></path>
        <path d="M5.43 11.28V9.81A2.91 2.91 0 018.32 6.9h10.25"></path>
      </svg>
    </button>

    <div class="right">
      <div class="selection-box">
        <div class="curr-selection s-right">

          <div class="curr-flag">
            <img class="curr-flag-image" src="https://valuta.exchange/img/flags/eur-flag.png" alt="flag">
          </div>

          <span class="curr-name">
          EUR - Euro
          </span>
          <svg viewBox="0 0 24 24" fill="none" stroke="#1c1c1c" stroke-linecap="round" stroke-linejoin="round"
               stroke-width="2px">
            <path class="prefix__a" d="M15.28 14.88L12 18.13l-3.28-3.25M8.72 9.12L12 5.87l3.25 3.25"></path>
          </svg>

          <div class="selector-container d-none">
            <div class="curr-search">
              <svg viewBox="0 0 24 24" fill="none" stroke="#808FA1" stroke-linecap="round" stroke-linejoin="round"
                   stroke-width="2px">
                <circle cx="10.54" cy="10.54" r="5.11"></circle>
                <line x1="18.58" y1="18.58" x2="14.19" y2="14.19"></line>
              </svg>
              <input id="right-search-input" type="text" placeholder="Search" class="curr-search-input">
            </div>
            <div class="curr-list">
<#--              <#list countries as item>-->
<#--                <button data-value="${item.code}" class="curr-list-item">-->
<#--                  <div data-value="${item.code}" class="item-flag">-->
<#--                    <img data-value="${item.code}" class="item-flag-img" src="${item.img}" alt="flag">-->
<#--                  </div>-->
<#--                  <span data-value="${item.code}" class="item-name">-->
<#--                  ${item.code} - ${item.name}-->
<#--                </span>-->
<#--                </button>-->
<#--              </#list>-->


            </div>
          </div>
        </div>

      </div>

      <input id="right-input" type="text" value="0" readonly class="curr-input">

      <div id="right-curr-display" class="curr-display">€</div>

    </div>


  </div>
  <div class="updated-time-container mt-5">
    Exchange rate <span id="exchange-txt">ALL/AFN</span>
    <span id="exchange-rate" class="exchange-rate">0.70102</span>
    updated at <span id="exchange-updated-at"></span> requested at <span id="requested-at"></span>

  </div>
</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="/app/app.js"></script>
</body>
</html>
