var currencies

var source = ""
var currency = ""
var timestamp = ""
var quotes = ""

var sourceValue = 1
var currencyValue = 0

var time_out


$('.selection-box .s-left').click(function (e) {
  console.log(e)
  if (e.target.classList.contains('curr-search-input'))
    return
  var currency_code = $(e.target).data('value')
  console.log(currency_code)
  if (currency_code) {
    source = currency_code
    if (currency.length > 0) {
      if (time_out) clearTimeout(time_out)
      send()
    }
    createCurr(source, '.left')
    updateTitle(source + " to " + currency)
  }

  let left = $(".left .selector-container")
  let right = $(".right .selector-container")
  left.toggleClass('d-none')
  right.addClass('d-none')
})

$('.selection-box .s-right').click(function (e) {
  console.log(e)
  if (e.target.classList.contains('curr-search-input'))
    return

  var currency_code = $(e.target).data('value')
  console.log(currency_code)
  if (currency_code) {
    currency = currency_code
    if (source.length > 0) {
      if (time_out) clearTimeout(time_out)
      send()

    }
    createCurr(currency, '.right')
    updateTitle(source + " to " + currency)
  }

  let left = $(".left .selector-container")
  let right = $(".right .selector-container")
  right.toggleClass('d-none')
  left.addClass('d-none')
})


var settings = {
  "async": true,
  "crossDomain": true,
  "url": "https://wft-geo-db.p.rapidapi.com/v1/geo/countries",
  "method": "GET",
  "headers": {
    "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
    "x-rapidapi-key": "0976d8a05amshf5bcc9d41419ec7p15154djsn227e94979fbd"
  }
}

var currency_url = {
  'url': 'http://localhost:8080/api/currency',
  'method': 'GET',
  'async': true
}

var converterApi = {
  'url': 'http://localhost:8080/api/convert/live',
  'method': 'GET',
  'data': {
    'currency': 'UZS',
    'source': 'USD',
  }
}


$(document).ready(function () {


  $.ajax(currency_url).done(function (responce) {
    if (responce['success']) {
      currencies = responce['currencies'];

      createCurrencyItems(currencies, '.left');
      createCurrencyItems(currencies, '.right');
      source = 'USD'
      currency = 'UZS'

      if (time_out) clearTimeout(time_out)
      send()

      createCurr(source, '.left')
      createCurr(currency, '.right')

      updateTitle(source + " to " + currency)
    } else {
      console.log(responce['error'])
    }
  })



  // $.ajax(converterApi).done(function (responce) {
  //   source = responce['source']
  //   timestamp = responce['timestamp']
  //   quotes = responce['quotes'][responce['key']]
  //
  //   console.log("source", source)
  //   console.log("timestamp", timestamp)
  //   console.log('quotes', quotes)
  // });
})

function send() {

  xhr = $.ajax({
    'url': 'http://localhost:8080/api/convert/live',
    'method': 'GET',
    'data': {
      'currency': currency,
      'source': source,
    }
  }).done(function (responce) {
    if(responce['success']){
      source = responce['source']
      timestamp = responce['timestamp']
      requested_at = responce['requested_at']
      quotes = responce['quotes'][responce['key']]

      console.log("source", source)
      console.log("timestamp", timestamp)
      console.log('quotes', quotes)

      updateFooter(quotes, timestamp, requested_at);


      calculate(sourceValue, currencyValue, quotes)
      if (time_out) clearTimeout(time_out)

      time_out = setTimeout(function () {
        send();
      }, 15000);
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: responce['error']['info'],
        footer: '<a href>Why do I have this issue?</a>'
      }).then((result) => {
        location.reload()
      })

    }

  }).fail(function (jqXHR, textStatus, errorThrown) {
    console.log("error")
  });
}

function calculate(sv, cv, quotes) {
  var _c = (sv * quotes).toFixed(2)


  $('#right-input').val(_c + "")


}

function updateTitle(text) {
  $('.title').text(text)
}

function updateFooter(quotes, timestamp, requested){
  updated_at = new Date(timestamp * 1000)
  requested_at = new Date(requested)

  var u_day = updated_at.getDate();
  var u_month = updated_at.getMonth();
  var u_year = updated_at.getFullYear();

  var u_hour = updated_at.getHours();
  var u_min = updated_at.getMinutes();
  var u_sec = updated_at.getSeconds();

  var r_day = requested_at.getDate();
  var r_month = requested_at.getMonth();
  var r_year = requested_at.getFullYear();

  var r_hour = requested_at.getHours();
  var r_min = requested_at.getMinutes();
  var r_sec = requested_at.getSeconds();

  $('#exchange-updated-at').text(u_year + "." + u_month + "." + u_day + " " + u_hour + ":" + u_min + ":" + u_sec)
  $('#requested-at').text(r_year + "." + r_month + "." + r_day + " " + r_hour + ":" + r_min + ":" + r_sec)
  $('#exchange-txt').text(source + "/" + currency)
  $("#exchange-rate").text(quotes)
}

$("#left-search-input").on("keyup", function (e) {
  var query = $(this).val()

  if (query.length > 0) {
    var searchResult = currencies.filter(el => el.code.startsWith(query.toUpperCase()));
    console.log("searchResult", searchResult)

    createCurrencyItems(searchResult, '.left')
  } else {
    createCurrencyItems(currencies, '.left')
  }

})

$("#right-search-input").on("keyup", function (e) {
  var query = $(this).val()

  if (query.length > 0) {
    var searchResult = currencies.filter(el => el.code.startsWith(query.toUpperCase()));
    console.log("searchResult", searchResult)

    createCurrencyItems(searchResult, '.right')
  } else {
    createCurrencyItems(currencies, '.right')
  }

})


$("#right-input").on("keypress keyup blur", function (event) {
  //this.value = this.value.replace(/[^0-9\.]/g,'');
  event.preventDefault();

  // if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) ||
  //   $(this).val().indexOf('.') != -1 && $(this).val().length - $(this).val().indexOf('.') > 2) {
  //   event.preventDefault();
  // }
  //
  // $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
  //
  // if ($(this).val() === "") {
  //   $(this).val(0)
  // }
  //
  // if ($(this).val().indexOf('.') == -1)
  //   $(this).val(parseFloat($(this).val()))
  //
  //
  // currencyValue = parseFloat($(this).val()).toFixed(2);
  //
  // // if(time_out) clearTimeout(time_out)
  // // send()
  // console.log("currencyValue", currencyValue)

});

$("#right-input").focusout(function (e) {
  if ($(this).val() === "0")
    return
  $(this).val(parseFloat($(this).val()).toFixed(2))
  $(this).val(parseFloat($(this).val()))

  currencyValue = parseFloat($(this).val()).toFixed(2)

  // if(time_out) clearTimeout(time_out)
  // send()
  console.log("currencyValue", currencyValue)
})


$("#left-input").on("keypress keyup blur", function (event) {
  //this.value = this.value.replace(/[^0-9\.]/g,'');
  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) ||
    $(this).val().indexOf('.') != -1 && $(this).val().length - $(this).val().indexOf('.') > 2) {
    event.preventDefault();
  }

  $(this).val($(this).val().replace(/[^0-9\.]/g, ''));

  if ($(this).val() === "") {
    $(this).val(0)
  }

  if ($(this).val().indexOf('.') == -1)
    $(this).val(parseFloat($(this).val()))

  sourceValue = parseFloat($(this).val()).toFixed(2)

  if (time_out) clearTimeout(time_out)
  send()
  console.log("sourceValue", sourceValue)


});

$("#left-input").focusout(function (e) {
  if ($(this).val() === "0")
    return

  $(this).val(parseFloat($(this).val()).toFixed(2))
  $(this).val(parseFloat($(this).val()))

  sourceValue = parseFloat($(this).val()).toFixed(2)
  sourceValue = parseFloat($(this).val()).toFixed(2)

  if (time_out) clearTimeout(time_out)
  send()
  console.log("sourceValue", sourceValue)
})


function createCurrencyItems(currencies, cls) {
  $(cls + ' .curr-list').empty()

  for (var i = 0; i < currencies.length; i++) {
    var item = currencies[i]
    console.log(item)

    var el = "<button data-value=\"" + item.code + "\" class=\"curr-list-item\">\n" +
      "                  <div data-value=\"" + item.code + "\" class=\"item-flag\">\n" +
      "                    <img data-value=\"" + item.code + "\" class=\"item-flag-img\" src=\"" + item.img + "\" alt=\"flag\">\n" +
      "                  </div>\n" +
      "                  <span data-value=\"" + item.code + "\" class=\"item-name\">\n" +
      "                  " + item.code + " - " + item.name + "" +
      "                </span>\n" +
      "                </button>"

    $(cls + ' .curr-list').append(el)
  }

}

function createCurr(curr, id) {
  if (curr) {
    var obj = currencies.find(el => el.code == curr)
    $(id + ' .curr-flag-image').attr('src', obj.img)
    $(id + ' .curr-name').text(obj.code + " - " + obj.name);
    var descr_text = obj.name + " (" + obj.code + ")"
    if (id === '.left') {
      $('#source-descr').text(descr_text)
      var txt = ""
      if (obj.symbol_native !== "") {
        txt = obj.symbol_native
      } else if (obj.symbol !== "") {
        txt = obj.symbol
      } else {
        txt = obj.code
      }
      $('#left-curr-display').text(txt)


    } else if (id === '.right') {
      $('#currency-descr').text(descr_text)
      var txt = ""
      if (obj.symbol_native !== "") {
        txt = obj.symbol_native
      } else if (obj.symbol !== "") {
        txt = obj.symbol
      } else {
        txt = obj.code
      }
      $('#right-curr-display').text(txt)
    }
  } else {
    console.log('Curr is not find.')
  }
}

